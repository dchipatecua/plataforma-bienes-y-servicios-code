<?php 
include "config.php";
include "utilidades.php";
$dbConn =  connect($db);


if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
    if (isset($_GET['id_dep']))
    {
      //Mostrar un post
      $sql = $dbConn->prepare("SELECT * FROM producto where idProducto=".$_GET['idProducto']."");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll());
      exit();
    }
    else 
    {
      //Mostrar lista de post
      $sql = $dbConn->prepare("SELECT * FROM producto");
      $sql->execute();
      $sql->setFetchMode(PDO::FETCH_ASSOC);
      header("HTTP/1.1 200 OK");
      echo json_encode( $sql->fetchAll());
      exit();
    }
}


//Actualizar
if ($_SERVER['REQUEST_METHOD'] == 'PUT')
{  
  parse_str(file_get_contents('php://input'), $request_params);
   $sql = "
          UPDATE producto
          SET nombre='".$request_params['nombre']."', descripcion='".$request_params['descripcion']."' WHERE idProducto='".$request_params['idProducto']."'";
   $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $request_params);
    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();
}
//Borrar
if ($_SERVER['REQUEST_METHOD'] == 'DELETE')
{  parse_str(file_get_contents('php://input'), $request_params);
   $sql = "
          DELETE FROM producto
          WHERE idProducto='".$request_params['idProducto']."'";
   $statement = $dbConn->prepare($sql);
    bindAllValues($statement, $request_params);
    $statement->execute();
    header("HTTP/1.1 200 OK");
    exit();

}
//En caso de que ninguna de las opciones anteriores se haya ejecutado
header("HTTP/1.1 400 Bad Request");
?>